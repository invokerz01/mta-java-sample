package com.company.samplemta.SampleJavaMTA.models;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Feed {
	
	private ObjText id;
	
	private List<Entry> entry;
    
    public ObjText getId() {
        return id;
    }

    public void setId(ObjText id) {
        this.id = id;
    }

	
    public List<Entry> getEntry() {
        return entry;
    }

    public void setEntry(List<Entry> entries) {
        this.entry = entries;
    }
}
