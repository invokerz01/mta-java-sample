package com.company.samplemta.SampleJavaMTA.controllers;

import org.springframework.http.*;
import java.util.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import com.company.samplemta.SampleJavaMTA.models.Feed;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;
import java.nio.charset.*;

@Controller
public class ProductsController {
	private static final Logger log = LoggerFactory.getLogger(HelloController.class);

  @GetMapping("/products")
  String hello(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
	RestTemplate restTemplate = new RestTemplate();
	String uri = "https://p2001180415trial-trial.apim1.hanatrial.ondemand.com/p2001180415trial/scpdemo/SalesOrderSet";
	
	String plainCreds = "";
	byte[] plainCredsBytes = plainCreds.getBytes();
	byte[] base64CredsBytes = Base64.getEncoder().encode(plainCredsBytes);
	String base64Creds = new String(base64CredsBytes);
	
	HttpHeaders headers = new HttpHeaders();
	headers.add("Authorization", "Basic " + base64Creds);
	
	HttpEntity<String> request = new HttpEntity<String>(headers);
	ResponseEntity<Feed> response = restTemplate.exchange(uri, HttpMethod.GET, request, Feed.class);
	Feed feed = response.getBody();
	log.info("Status"+response.getStatusCode());
	//Feed feed = restTemplate.getForObject(uri, HttpMethod.GET, new HttpEntity<Feed>(createHeaders(username, password)), Feed.class);
    
    model.addAttribute("feed", feed);
    //log.info(master.toString());
    //log.info(feed.toString());
    //log.info(quote.toString());

    return null;
  }
  
}
