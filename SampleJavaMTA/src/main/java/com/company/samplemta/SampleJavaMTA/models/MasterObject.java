package com.company.samplemta.SampleJavaMTA.models;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MasterObject {
	
	@JsonProperty("feed")
	private Feed feed;
	
	public MasterObject() {
    }
	
	
	public void setFeed(Feed feed){
		this.feed = feed;
	}
	
	public Feed getFeed(){
		return this.feed;
	}
}
