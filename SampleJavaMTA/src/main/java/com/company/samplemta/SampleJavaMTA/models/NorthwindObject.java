package com.company.samplemta.SampleJavaMTA.models;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NorthwindObject {
	
	@JsonProperty("odata.metadata")
	private String metadata;
	
	@JsonProperty("value")
	private List<ValueV2> value;
	
	public NorthwindObject(){
		
	}
	
	public void setMetadata(String metadata){
		this.metadata = metadata;
	}
	
	public String getMetadata(){
		return metadata;
	}
	
	public void setValue(List<ValueV2> value){
		this.value = value;
	}
	
	public List<ValueV2> getValue(){
		return value;
	}
}
