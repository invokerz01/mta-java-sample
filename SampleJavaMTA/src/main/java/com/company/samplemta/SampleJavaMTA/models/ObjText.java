package com.company.samplemta.SampleJavaMTA.models;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ObjText {
	
@JsonProperty("TEXT")
private String TEXT;

public ObjText() {
    }
	
	
	public void setText(String TEXT){
		this.TEXT = TEXT;
	}
	
	public String getText(){
		return this.TEXT;
	}
}
