package com.company.samplemta.SampleJavaMTA.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;
import com.company.samplemta.SampleJavaMTA.models.NorthwindObject;
import org.springframework.ui.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class ApiDataController {
private static final Logger log = LoggerFactory.getLogger(HelloController.class);
  @GetMapping("/apidata")
  String apidata(Model model) {
	RestTemplate restTemplate = new RestTemplate();
	String uri = "https://services.odata.org/v3/northwind/northwind.svc?$format=json";
	
	 NorthwindObject northwindObj = restTemplate.getForObject(uri, NorthwindObject.class);
    model.addAttribute("northwind", northwindObj);
		
    return null;
  }
}
