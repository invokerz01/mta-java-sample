package com.company.samplemta.SampleJavaMTA.models;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Entry {
	
	@JsonProperty("id")
	private ObjText id;
	
	@JsonProperty("title")
	private ObjText title;
	
	@JsonProperty("updated")
	private ObjText updated;
	
	public Entry() {
    }
	
	public void setId(ObjText id){
		this.id = id;
	}
	
	public ObjText getId(){
		return this.id;
	}
	
	public void setTitle(ObjText title){
		this.title = title;
	}
	
	public ObjText getTitle(){
		return this.title;
	}
	
	public void setUpdated(ObjText updated){
		this.updated = updated;
	}
	
	public ObjText getUpdated(){
		return this.updated;
	}

}
